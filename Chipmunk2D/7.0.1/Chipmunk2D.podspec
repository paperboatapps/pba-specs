Pod::Spec.new do |s|
  s.name         = "Chipmunk2D"
  s.version      = "7.0.1"
  s.summary      = "Chipmunk is a simple, lightweight, fast and portable 2D rigid body physics library written in C."
  s.homepage     = "http://chipmunk-physics.net"
  s.license      = 'MIT '
  s.author       = { "Scott Lembcke" => "admin@howlingmoonsoftware.com" }
  s.public_header_files   = "include/**/*.h"
  s.source       = { :http => 'http://chipmunk-physics.net/release/Chipmunk-7.x/Chipmunk-7.0.1.tgz'}
  s.source_files = 'src/**/*.{h,c}', 'include/chipmunk/**/*.{h,c}'
  s.preserve_paths    = 'include/', 'objectivec/include/**'
  s.header_mappings_dir = 'include/'
  s.subspec 'ObjectiveC' do |so|
  	so.requires_arc = false
  	so.source_files = 'objectivec/**/*.{h,m}'
  	so.public_header_files = 'objectivec/include/**/*.h'
  	so.header_mappings_dir = 'objectivec/include/'
  	so.xcconfig = {'OTHER_CFLAGS' => '-Wno-documentation'}
  end
end
