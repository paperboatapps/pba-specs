#
# Be sure to run `pod lib lint cocos2dx.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'cocos2dx'
  s.version          = '3.17.11'
  s.summary          = 'cocos2dx prebuilt libs'

  s.description      = <<-DESC
  	This is the pod for prebuilt cocos2dx
                       DESC

  s.homepage         = 'https://www.paperboatapps.com'
  s.license          = {:type => 'Copyright',:text => 'Copyright Paper Boat Apps'}
  s.author           = { 'Anupam Dhanuka' => 'anupam@paperboatapps.com' }
  s.source			 = { :http => 'https://bitbucket.org/paperboatapps/cocos2d-x/downloads/libcocos2dx_3.17.11.zip' }
  
  s.platform		 = :ios
  s.ios.deployment_target = '9.1'
  #s.source_files    = 'include/**/*.{h,inl}'
  s.preserve_paths    = 'include/**/*.{h,inl}','lib/*'
  s.public_header_files    = 'include/**/*.{h,inl}'
  s.header_mappings_dir = 'include/'
  s.ios.vendored_libraries = 'lib/libcocos2dx.a'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => "${PODS_ROOT}/Headers/Public/cocos2dx/cocos/ ${PODS_ROOT}/Headers/Public/cocos2dx/cocos/audio/include ${PODS_ROOT}/Headers/Public/cocos2dx/cocos/audio/ios ${PODS_ROOT}/Headers/Public/cocos2dx/cocos/editor-support/ ${PODS_ROOT}/Headers/Public/cocos2dx/cocos/editor-support/spine/include/ ${PODS_ROOT}/Headers/Public/cocos2dx/cocos/editor-support/spine/src/ ${PODS_ROOT}/Headers/Public/cocos2dx/external/ ${PODS_ROOT}/Headers/Public/cocos2dx/external/gaf ${PODS_ROOT}/Headers/Public/cocos2dx/external/Box2D/include",
  				 'OTHER_CFLAGS' => '-Wno-documentation'}
  s.ios.frameworks = 'UIKit','OpenGLES', 'CoreGraphics','QuartzCore','CoreText', 'CoreMotion', 'GameController','OpenAL','AudioToolbox','AVFoundation','MediaPlayer' 
  s.ios.libraries = 'z','iconv'
end
