#
# Be sure to run `pod lib lint cocos2d-x.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'cocos2d-x'
  s.version          = '3.17.4'
  s.summary          = 'cocos2d-x prebuilt libs'

  s.description      = <<-DESC
  	This is the pod for prebuilt cocos2d-x
                       DESC

  s.homepage         = 'https://www.paperboatapps.com'
  s.license          = {:type => 'Copyright',:text => 'Copyright Paper Boat Apps'}
  s.author           = { 'Anupam Dhanuka' => 'anupam@paperboatapps.com' }
  s.source			 = { :http => 'https://bitbucket.org/paperboatapps/cocos2d-x/downloads/libcocos2d-x_3.17.4.zip' }
  
  s.platform		 = :ios
  s.ios.deployment_target = '9.1'
  #s.source_files    = 'include/**/*.{h,inl}'
  s.preserve_paths    = 'include/**/*.{h,inl}','lib/*'
  s.public_header_files    = 'include/**/*.{h,inl}'
  s.header_mappings_dir = 'include/'
  s.ios.vendored_libraries = 'lib/libcocos2d-x.a'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => "${PODS_ROOT}/Headers/Public/cocos2d-x/cocos/ ${PODS_ROOT}/Headers/Public/cocos2d-x/cocos/audio/include ${PODS_ROOT}/Headers/Public/cocos2d-x/cocos/audio/ios ${PODS_ROOT}/Headers/Public/cocos2d-x/cocos/editor-support/ ${PODS_ROOT}/Headers/Public/cocos2d-x/external/**",
  				 'OTHER_CFLAGS' => '-Wno-documentation'}
  s.ios.frameworks = 'UIKit','OpenGLES', 'CoreGraphics','QuartzCore','CoreText', 'CoreMotion', 'GameController','OpenAL','AudioToolbox','AVFoundation','MediaPlayer' 
  s.ios.libraries = 'z','iconv'
end
