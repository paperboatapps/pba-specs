#
# Be sure to run `pod lib lint cocos2d-x.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'cocos2d-x'
  s.version          = '3.15.0'
  s.summary          = 'cocos2d-x prebuilt libs'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  	This is the pod for prebuilt cocos2d-x
                       DESC

  s.homepage         = 'https://www.paperboatapps.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Anupam Dhanuka' => 'anupam@paperboatapps.com' }
  s.source           = { :git => 'ssh://git@bitbucket.org/paperboatapps/cocos2d-x.git', :tag => "prebuilt_3.15.0" }

  s.platform		 = :ios
  s.ios.deployment_target = '8.0'
  s.source_files    = 'cocos/**/*.h'
  s.public_header_files    = 'cocos/**/*.h'
  s.ios.vendored_libraries = 'prebuilt/ios/libcocos2d-x.a'

end
