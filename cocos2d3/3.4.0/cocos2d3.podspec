#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'cocos2d3'
  s.version          = '3.4.0'
  s.summary          = "cocos2d iphone 3.x prebuilt lib"
  s.description      = <<-DESC
  	This is the pod for prebuilt cocos2d objC 3.x
                       DESC
  s.homepage         = 'https://www.paperboatapps.com'
  s.license          = {:type => 'Copyright',:text => 'Copyright Paper Boat Apps'}
  s.author           = { 'Anupam Dhanuka' => 'anupam@paperboatapps.com' }
  #s.source           = { :http => 'http://localhost/~Anupam/libcocos2d3.zip'}
  s.source           = { :git => 'git@bitbucket.org:paperboatapps/cc2d3-prebuilt.git'}

  s.platform		 = :ios
  s.ios.deployment_target = '8.1'
  s.preserve_paths    = 'include/**','lib/*'
  s.public_header_files    = 'include/*.h','include/Platforms/*.h','include/Platforms/iOS/*.h','include/Support/*.h'
  s.header_mappings_dir = 'include/'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => "${PODS_ROOT}/Headers/Public/cocos2d3/**"}
  s.ios.vendored_libraries = 'lib/libcocos2d3.a'
  
  s.dependency 'Chipmunk2D'
end
